# CONTRIBUTING GUIDE LINES

## How to setup a dev enviroment (Debian)?
* Install the dependencies
```bash
$ sudo apt install python3-dev python3-venv git

$ sudo apt install postfix # install on with 'local' option
```
* Create a dedicated user
```bash
$ sudo adduser lythyr

$ sudo su - lythyr
```
* Clone M3 git repo
```bash
$ git clone git@gitlab.com:mailman/mailman.git
```
* Create a virtual environment
```bash
$ pyvenv mm3-env
```
* Update `.bashrc` to automatically activate the virtualenv when login as `lythyr` user
```bash
$ echo "source mm3-env/bin/activate" >> .bashrc
```
* Activate the virtual environment
```bash
$ source mm3-env/bin/activate
```
* Install M3 dependencies
```bash
$ cd mailman

$ python3 setup.py develop
```
* Create a default config file for M3
```bash
$ cp src/mailman/config/schema.cfg mailman.cfg
```
* Start M3
```bash
$ ../mm3-env/bin/mailman start
```
