# lythyr

Encrypted mailing list and remailer inspired by [schleuder2](https://schleuder2.nadir.org/).

[lythyr](https://en.wiktionary.org/wiki/lythyr) means letter in [Welsh](https://en.wikipedia.org/wiki/Welsh_language).

# State of the project

 * *Exploratory research.*

# Current ideas

 * Follow the [schleuder](https://schleuder2.nadir.org/) way of doing things.
 * Implement in python3.
 * Integrate with [Mailman3](https://gitlab.com/calafou/lythyr/issues/1).
 * Explore useful/interesting features such as [memory hole](https://gitlab.com/calafou/lythyr/issues/3).

# Dependencies

 * `python3-gnupg`?
 * `mailman3`?





