# Specifications for lythyr

## Use case
A group of people without high-sec ambitions want to use their GPG web of trust to communicate between each other via a mailing-list alike interface.

## Core Features

### Command system
The users of a list should be able to interact with its configuration without necessarily access the machine where the list is hosted.
The way it is actually implemented in schleuder2 is to parse the first line of an email and check against the presence of a specific prefix.
Let's divide the commands in two privilege levels:

#### Mantainer level
The mantainer should be able to do:
* CRUD ooperations on email-fingerprint pair values, written in a config file.

#### User level
* subscribe / unsubscribe to the list.

### Encryption
Each user should be able to send an email to the mailing list that will be decrypted by the server and re-encrypted using the public-keys of the users specified in the config file. Finally, this email will be re-sent to the members of the list.
Each user should be able to receive an encrypted email and decrypt it.

## Extra Features

### Receive internally to a list, emails from outside of the list
A group of users should be able to decide if they want to accept external unencrypted emails to be encrypted and distirbuted through the mailing list.

## Technical Features

### Memory hole support
Stash relevant headers within the body of the e-mail in a standardized way.

### Proxy re-encryption ?

## Implementation details

### Option 1: mailman3-lythyr
Write a set of modules that could extend core functionalities.

### Option 2: set of MR to the Mailman3 Core
Prepare concise set of MR to submit to Mailman3 Core.

### Option 3: hybrid between O1 and O2
If something can be modularized it should be modularized, for everything else try to collaborate with Mailman3 developers to contribute to core functionalities.






