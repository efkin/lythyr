#!/usr/bin/python3
# Testing client for lythyr.
# It accomplish the function of exim4/postfix.
# Just for development purposes.


import smtplib
import email.utils
from email.mime.text import MIMEText

# Create testing message
msg = MIMEText('Soon this mesage is gonna be signed and encrypted.')
msg['To'] = email.utils.formataddr(('Recipient', 'list@lythyr.calafou.org'))
msg['From'] = email.utils.formataddr(('Author', 'brave@author.me'))
msg['Subject'] = 'Hello World!'

server = smtplib.SMTP('127.0.0.1', 1025)
server.set_debuglevel(True) # show communication with the server

try:
    server.sendmail('brave@author.me', ['list@lythyr.calafou.org'], msg.as_string())
finally:
    server.quit()

